#!/bin/bash 

TOUT="5"

TimeCalc(){
	
	ETIME=$(date +"%s%N")
	TIME=$(($ETIME - $1))
	TIME=$(($TIME/10000000))
	segTIME=$(($TIME/100))
	nanoTIME=$(($TIME%100))
	if [ "$nanoTIME" -lt "10" ];then 
		nanoTIME="0$nanoTIME"
	fi
	TTIME="$segTIME,$nanoTIME s"
	echo "$TTIME"

}



if [ "$#" -eq "0" ];
then
	echo "Servers file required as parameter"
	exit
elif [ ! -r $1 ];
then
	echo "Servers file doesn't exist"
	exit
fi

FILE1="$1"
RESULT="None"
echo "- Server:Port    |    Warning    |   Time"
while read SERVER
do
	if [ -n $SERVER ]; then
	JUMP=$(echo "$SERVER" | awk '{print substr($0,0,1)}')
	if [ "$JUMP" != '#' ]; then	
	STIME=$(date +"%s%N")	

	RESULT=$SERVER;
	
	DOMAIN=$( echo "$SERVER" | awk -F':' '{print $1}')
	PORT=$( echo "$SERVER" | awk -F':' '{print $2}')
	
	RESPONSE=$(host "$DOMAIN" | awk '/pointer/ { print $5 }')
	if [ ! -z "$RESPONSE" ];
	then
		DOMAIN=$RESPONSE
	fi
	RESPONSE=$(host "$DOMAIN" | awk '/has address/ { print $4 }')

	if [  -z "$RESPONSE" ];
	then
		RESULT="$RESULT | DNS record does not exist"
	else
		#echo "DNS: OK"
#nc -z -v -w5 $DOMAIN $PORT
		OPEN=$(nc -w$TOUT -z -v $DOMAIN $PORT 2>&1);

		if [ -z "$OPEN" ]; then
			RESULT="$RESULT | Service not running"
		else
			#echo "Port: OK"
			if echo "$OPEN" | grep -q "Network is unreachable"; 
			then
				RESULT="$RESULT | Service not running"
			elif echo "$OPEN" | grep -q "Connection refused"; 
			then
			  	RESULT="$RESULT | Connection rejected"
			elif echo "$OPEN" | grep -q "timed out";
			then
  				RESULT="$RESULT | Timeout"
			elif echo "$OPEN" | grep -q "succeeded!";
			then
				RESULT="$RESULT | Success"
			fi	
		fi	
	fi
	
	
	TTIME=$(TimeCalc "$STIME")
	
	echo "$RESULT | $TTIME"
	fi
	fi

done < "$FILE1"





# Success.
# DNS record does not exist. (cannot resolve hostname)
# Service not running (remote host is not listening on that port)
# Connection rejected (remote host is listening on that port but network connection is rejected)
#- Timeout (remote host is listening on that port but network connection is timing out)




#server-1.dev.internal:443 | Success | 3.4s
#server-2.dev.internal:25 | Service not running | 0.2s