This is a Linux Shell Script to test conections to servers port list from file by parameter.



<pre>:~$./netcheck listserver.txt</pre>



<strong>content of listserver.txt</strong>

google.com:80

google.com:81

google.com:21

blibli:80

#blibli:23

#blibli:21

#google.es:80

4130bikecompany.com:80

4130bikecompany.com:22

4130bikecompany.com:21

172.26.0.1:80

172.26.0.1:81

172.26.0.1:22

172.26.0.1:21




* The # simbol skip the line




<strong>Posible output values:</strong>

 Success.
 
 DNS record does not exist. (cannot resolve hostname)
 
 Service not running (remote host is not listening on that port)
 
 Connection rejected (remote host is listening on that port but network connection is rejected)
 
 Timeout (remote host is listening on that port but network connection is timing out)





<strong>Output examples:</strong>

 server-1.dev.internal:443 | Success | 3.4s
 
 server-2.dev.internal:25 | Service not running | 0.2s
 